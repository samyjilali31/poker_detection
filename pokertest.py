import random




jeu = True

cartetab = ['2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '1♣', 'V♣', "D♣", "R♣", "A♣", '2♥', '3♥', '4♥', '5♥', '6♥',
            '7♥',
            '8♥', '9♥', '1♥', "V♥", "D♥", "R♥", "A♥", '2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '1♠', "V♠", "D♠",
            "R♠", "A♠", '2♦'
    , '3♦', '4♦', '5♦', '6♦', '7♦', '8♦', '9♦', '1♦', "V♦", "D♦", "R♦", "A♦"]

suithaute = ['A', 'V', 'D', 'R']
hautepaire = ['A', 'V', 'D', 'R', '10']




# Fonction genérer cartes aléatoires
def random_value():
    return random.choice(cartetab)


class OnBoard:
    # Fonction appliquer une nouvelle carte aléatoire par tirage
    def __init__(self, carte=""):
        self.carte = random_value()
        # Enlever la carte tirée du paquet
        i = cartetab.index(self.carte)
        del cartetab[i]
        print(f'Cartes {self.carte} ')

    # Fonction récupérer le premier caractère de la carte
    def get_Char(self):
        return self.carte[0]

    def get_Color(self):
        return self.carte[1]

print("\033[34mJoueur1\033[0m")
carte1 = OnBoard()
carte2 = OnBoard()
print("\033[34mJoueur2\033[0m")
carte8 = OnBoard()
carte9 = OnBoard()
print("\033[34mJoueur3\033[0m")
carte10 = OnBoard()
carte11 = OnBoard()
print("\033[34mJoueur4\033[0m")
carte12 = OnBoard()
carte13 = OnBoard()

class OnGame:

    def __init__(self, pot, banque, blinde, mise, base):

        # Création du jeu: pot,banque,blinde
        self.base = 500
        self.banque = 0
        self.blinde = 25
        self.pot = self.blinde
        self.mise = 0

        print('Votre Banque ', self.base - self.blinde - self.mise)
        print('Mise obligatoire: ', self.blinde)
        print('Pot ', self.pot)

    def get_blinde(self, x, y, c1, c2):
        # Jouer les blindes ou non en fonction des mains
        cardhight = False
        cartepaire = False
        s = False
        self.mise = False

        if x == y:
            for paire in hautepaire:
                if x == paire:
                    s += 1
                    print("\033[36mVous relancez 50% de votre banque!\033[0m", self.base)
                    self.mise = self.base * 50 / 100
                    self.pot = self.pot + self.mise
                    self.banque = self.base - self.mise
                    print('banque =', self.banque)
                    print("Mise: ", self.mise)
                    print('pot =', self.pot)
                    cartepaire += 1
                    self.blinde = self.mise

        if x == y:
            if cartepaire < 1:
                if cardhight < 1:
                    if self.blinde < 151:
                        s = + 1
                        print("\033[36mVous relancez (blinde fois 3)\033[0m")
                        self.mise = self.blinde * 3
                        self.blinde = self.mise
                        self.pot = self.pot + self.blinde
                        self.banque = self.base - self.mise
                        print('banque=', self.banque)
                        print("Mise: ", self.mise)
                        print('pot =', self.pot)
                        self.blind = self.mise

        for L in suithaute:
            if L == x:
                if cartepaire < 1:
                    if self.blinde < 76:
                        s += 1
                        print("\033[36mVous jouez la blinde\033[0m")
                        self.mise = self.blinde
                        self.pot = self.pot + self.blinde
                        self.banque = self.base - self.mise
                        print('banque=', self.banque)
                        print("Mise: ", self.mise)
                        print('pot =', self.pot)
                        cardhight += 1

            if L == y:
                if cardhight < 1:
                    if cartepaire < 1:
                     if self.blinde < 76:
                        self.mise = self.blinde
                        s += 1
                        print("\033[36mVous jouez la blinde\033[0m")
                        self.pot = self.pot + self.blinde
                        self.banque = self.base - self.mise
                        print('banque=', self.banque)
                        print("Mise: ", self.mise)
                        print('pot =', self.pot)

        if self.mise == 0:
            print("\033[36mVous passez.\033[0m")
            #self.mise = self.mise - 25

        elif c1 == c2 and s < 1:
            if self.blinde < 101:
                print("\033[36mVous jouez la blinde\033[0m")

                self.mise = self.blinde
                self.pot = self.pot + self.blinde
                self.banque = self.base - self.blinde
                print('banque=', self.banque)
                print("Mise: ", self.mise)
                print('pot =', self.pot)
                self.blind = self.mise

        

jeu = OnGame("", "", "", "", "")
jeu.get_blinde(carte1.get_Char(), carte2.get_Char(), carte1.get_Color(), carte2.get_Color())
jeu.get_blinde(carte8.get_Char(), carte9.get_Char(), carte8.get_Color(), carte9.get_Color())
jeu.get_blinde(carte10.get_Char(), carte11.get_Char(), carte10.get_Color(), carte11.get_Color())
jeu.get_blinde(carte12.get_Char(), carte13.get_Char(), carte12.get_Color(), carte13.get_Color())

a = input('LANCER CARTES AU FLOP?')

print("\033[34mCartes au Flop\033[0m")
carte3 = OnBoard()
carte4 = OnBoard()
carte5 = OnBoard()

suit = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '1', 'V', 'D', 'R', 'A']

liste = [carte1.get_Char(), carte2.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char()]

liste8 = [carte8.get_Char(), carte9.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char()]

liste10 = [carte10.get_Char(), carte11.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char()]

liste12 = [carte12.get_Char(), carte13.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char()]

fullliste = [liste,liste8,liste10,liste12]


liste2 = [carte1.get_Color(), carte2.get_Color(), carte3.get_Color(), carte4.get_Color()
    , carte5.get_Color()]

liste8c = [carte8.get_Color(), carte9.get_Color(), carte3.get_Color(), carte4.get_Color()
    , carte5.get_Color() ]

r = False
# Detecter une Suite
def get_Suite(x, y):
    
    return frozenset(x).intersection(y)
for es in fullliste:
 r +=1
 if r == 1:
    print('\033[34mSUITE JOUEUR 1:\033[0m')
 if r == 2:
    print('\033[34mSUITE JOUEUR 2:\033[0m')
 if r == 3:
    print('\033[34mSUITE JOUEUR 3:\033[0m')
 if r == 4:
    print('\033[34mSUITE JOUEUR 4:\033[0m') 
           

# Detecter l'indice de la suite(frozen)
 len(get_Suite(es, suit[0:6]))
# Afficher la suite
 if len(get_Suite(es, suit[0:5])) > 4:
    print('Suite de A à 5')
 if len(get_Suite(es, suit[1:6])) > 4:
    print('Suite de 2 à 6')
 if len(get_Suite(es, suit[2:7])) > 4:
    print('Suite de 3 à 7')
 if len(get_Suite(es, suit[3:8])) > 4:
    print('Suite de 4 à 8')
 if len(get_Suite(es, suit[4:9])) > 4:
    print('Suite de 5 à 9')
 if len(get_Suite(es, suit[5:10])) > 4:
    print('Suite de 6 à 10')
 if len(get_Suite(es, suit[6:11])) > 4:
    print('Suite de 7 à V')
 if len(get_Suite(es, suit[7:12])) > 4:
    print('Suite de 8 à D')
 if len(get_Suite(es, suit[8:13])) > 4:
    print('Suite de 9 à R')
 if len(get_Suite(es, suit[9:14])) > 4:
    print('Quinte Royale')

class TuchFlop1:
    # Initialisation
    def __init__(self, x, y, card3, card4, card5):

        self.x = x.get_Char()
        self.y = y.get_Char()
        self.card3 = carte3.get_Char()
        self.card4 = carte4.get_Char()
        self.card5 = carte5.get_Char()

    # Detecter une paire en main
    def get_Paire_en_main(self, list=['', '', '']):
        self.list = [self.card3, self.card4, self.card5]
        d = 0
        r = 0
        if (self.x == self.y):
            print('\033[34mPaire EN MAIN3\033[0m')
            d += 1
            if self.x == 'A':
                print("PAIRE d'AS!!")
            if self.x == 'D' or self.x == 'K':
                print("Paire haute en main!")
            for paire in self.list:
                if (self.x == paire):
                    print('\033[37mBRELAN\033[0m')
                    r += 1
                    if len(get_Suite(liste, suit)) == 3:
                        print('FULL1')
        if len(get_Suite(self.list, suit)) == 4:
            print("Paire au flop")
            d += 1
            r += 1
            if d > 1:
                print('Double paire(Main+Flop)')
                r += 1
            if r > 2:
                print('FULL4')

        if len(get_Suite(self.list, suit)) == 1:
            print("Brelan?")
        if len(get_Suite(self.list, suit)) == 2:
            print("Paire au flop")

    # Detecter une paire au flop
    def get_Paire(self, list=['', '', '']):
        x = False
        y = False
        b = False
        p = False
        c = False
        f = False
        self.list = [self.card3, self.card4, self.card5]
        for flop in self.list:
            if self.x == flop:
                print('\033[35mPAIRE\033[0m', self.x)
                x += 1
                b += 1
                # Detecter Double paire
                if b > 1:
                    print('\033[32mDOUBLE PAIRE1\033[0m')
                    p += 1
                    # Detecter Brelan
                if (x > 1):
                    print('\033[31mBRELAN1\033[0m')
                    c += 1
            if self.y == flop:
                print('\033[34mPAIRE\033[0m', self.y)
                y += 1
                b += 1
                if b > 1:
                    print('\033[32mDOUBLE PAIRE2\033[0m')
                    p += 1
                if (y > 1):
                    print('\033[31mBRELAN2\033[0m')
                    c += 1
                    if c > 1 and p < 3:
                        print('CARRE')
                    if p > 1:
                        print("FULL")
            if b > 2:
                print("\033[33mFULL\033[0m")
                f += 1
            if f > 4:
                print('CARRE')

class TuchColor1:

    def __init__(self, x, y, card3, card4, card5):
        self.x = x.get_Color()
        self.y = y.get_Color()
        self.card3 = carte3.get_Color()
        self.card4 = carte4.get_Color()
        self.card5 = carte5.get_Color()
        

    def get_Color_Hand(self, list=['', '', '']):
        self.list = [self.card3, self.card4, self.card5]
        if self.x == self.y:
            print('\033[36mCOULEUR en main\033[0m')

    def get_Color(self, list=['', '', '', '', '']):
        self.list = [self.x, self.y, self.card3, self.card4, self.card5]
        c = False
        s = False
        p = False
        t = False
        for couleur in self.list:
            if couleur == '♥':
                c += 1
                if c > 4:
                    print("\033[31mCouleur ♥\033[0m")
            if couleur == '♦':
                s += 1
                if s > 4:
                    print("\033[31mCouleur ♦\033[0m")
            if couleur == '♠':
                p += 1
                if p > 4:
                    print("\033[37mCouleur ♠\033[0m")
            if couleur == '♣':
                t += 1
                if t > 4:
                    print("\033[37mCouleur ♣\033[0m")

print('--------Tuch Player 1----------')
pairHand = TuchFlop1(carte1, carte2, '', '', '')
pair = TuchFlop1(carte1, carte2, '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop1(carte1, carte2, '', '', '')
couleur = TuchColor1(carte1, carte2, '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor1(carte1, carte2, '', '', '')
couleur5.get_Color()
suite = TuchFlop1(carte1, carte2, '', '', '')


print('--------Tuch Player 2----------')
pairHand = TuchFlop1(carte8, carte9, '', '', '')
pair = TuchFlop1(carte8, carte9, '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop1(carte8, carte9, '', '', '')
couleur = TuchColor1(carte8, carte9, '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor1(carte8, carte9, '', '', '')
couleur5.get_Color()
suite = TuchFlop1(carte8, carte9, '', '', '')


print('--------Tuch Player 3----------')
pairHand = TuchFlop1(carte10, carte11, '', '', '')
pair = TuchFlop1(carte10, carte11, '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop1(carte10, carte11, '', '', '')
couleur = TuchColor1(carte10, carte11, '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor1(carte10, carte11, '', '', '')
couleur5.get_Color()
suite = TuchFlop1(carte10, carte11, '', '', '')


print('--------Tuch Player 4----------')
pairHand = TuchFlop1(carte12, carte13, '', '', '')
pair = TuchFlop1(carte12, carte13, '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop1(carte12, carte13, '', '', '')
couleur = TuchColor1(carte12, carte13, '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor1(carte12, carte13, '', '', '')
couleur5.get_Color()
suite = TuchFlop1(carte12, carte13, '', '', '')
print('---------------------')


b = input ('LANCER DERNIERES CARTES DU FLOP?')
carte6 = OnBoard()
carte7 = OnBoard()


suit = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '1', 'V', 'D', 'R', 'A']

liste = [carte1.get_Char(), carte2.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char(), carte6.get_Char(), carte7.get_Char()]

liste8 = [carte8.get_Char(), carte9.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char(), carte6.get_Char(), carte7.get_Char()]

liste10 = [carte10.get_Char(), carte11.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char(), carte6.get_Char(), carte7.get_Char()]

liste12 = [carte12.get_Char(), carte13.get_Char(), carte3.get_Char(),
         carte4.get_Char(), carte5.get_Char(), carte6.get_Char(), carte7.get_Char()]

fullliste = [liste,liste8,liste10,liste12]

liste2 = [carte1.get_Color(), carte2.get_Color(), carte3.get_Color(), carte4.get_Color()
    , carte5.get_Color(), carte6.get_Color(), carte7.get_Color(), ]

liste8c = [carte8.get_Color(), carte9.get_Color(), carte3.get_Color(), carte4.get_Color()
    , carte5.get_Color(), carte6.get_Color(), carte7.get_Color(), ]

r = False
# Detecter une Suite
def get_Suite(x, y):
    
    return frozenset(x).intersection(y)
for es in fullliste:
 r +=1
 if r == 1:
    print('\033[34mSUITE JOUEUR 1:\033[0m')
 if r == 2:
    print('\033[34mSUITE JOUEUR 2:\033[0m')
 if r == 3:
    print('\033[34mSUITE JOUEUR 3:\033[0m')
 if r == 4:
    print('\033[34mSUITE JOUEUR 4:\033[0m') 
           

# Detecter l'indice de la suite(frozen)
 len(get_Suite(es, suit[0:6]))
# Afficher la suite
 if len(get_Suite(es, suit[0:5])) > 4:
    print('Suite de A à 5')
 if len(get_Suite(es, suit[1:6])) > 4:
    print('Suite de 2 à 6')
 if len(get_Suite(es, suit[2:7])) > 4:
    print('Suite de 3 à 7')
 if len(get_Suite(es, suit[3:8])) > 4:
    print('Suite de 4 à 8')
 if len(get_Suite(es, suit[4:9])) > 4:
    print('Suite de 5 à 9')
 if len(get_Suite(es, suit[5:10])) > 4:
    print('Suite de 6 à 10')
 if len(get_Suite(es, suit[6:11])) > 4:
    print('Suite de 7 à V')
 if len(get_Suite(es, suit[7:12])) > 4:
    print('Suite de 8 à D')
 if len(get_Suite(es, suit[8:13])) > 4:
    print('Suite de 9 à R')
 if len(get_Suite(es, suit[9:14])) > 4:
    print('Quinte Royale')

class TuchFlop:
    # Initialisation
    def __init__(self, x, y, card3, card4, card5, card6, card7):

        self.x = x.get_Char()
        self.y = y.get_Char()
        self.card3 = carte3.get_Char()
        self.card4 = carte4.get_Char()
        self.card5 = carte5.get_Char()
        self.card6 = carte6.get_Char()
        self.card7 = carte7.get_Char()

    # Detecter une paire en main
    def get_Paire_en_main(self, list=['', '', '', '', '']):
        self.list = [self.card3, self.card4, self.card5, self.card6, self.card7]
        d = 0
        r = 0
        if (self.x == self.y):
            print('\033[34mPaire EN MAIN3\033[0m')
            d += 1
            if self.x == 'A':
                print("PAIRE d'AS!!")
            if self.x == 'D' or self.x == 'K':
                print("Paire haute en main!")
            for paire in self.list:
                if (self.x == paire):
                    print('\033[37mBRELAN\033[0m')
                    r += 1
                    if len(get_Suite(liste, suit)) == 3:
                        print('FULL1')
        if len(get_Suite(self.list, suit)) == 4:
            print("Paire au flop")
            d += 1
            r += 1
            if d > 1:
                print('Double paire(Main+Flop)')
                r += 1
            if r > 2:
                print('FULL4')

        if len(get_Suite(self.list, suit)) == 3:
            print("Brelan ou double paire au Flop")
        if len(get_Suite(self.list, suit)) == 2:
            print("Full ou carré au flop")

    # Detecter une paire au flop
    def get_Paire(self, list=['', '', '', '', '']):
        x = False
        y = False
        b = False
        p = False
        c = False
        f = False
        self.list = [self.card3, self.card4, self.card5, self.card6, self.card7]
        for flop in self.list:
            if self.x == flop:
                print('\033[35mPAIRE\033[0m', self.x)
                x += 1
                b += 1
                # Detecter Double paire
                if b > 1:
                    print('\033[32mDOUBLE PAIRE1\033[0m')
                    p += 1
                    # Detecter Brelan
                if (x > 1):
                    print('\033[31mBRELAN1\033[0m')
                    c += 1
            if self.y == flop:
                print('\033[34mPAIRE\033[0m', self.y)
                y += 1
                b += 1
                if b > 1:
                    print('\033[32mDOUBLE PAIRE2\033[0m')
                    p += 1
                if (y > 1):
                    print('\033[31mBRELAN2\033[0m')
                    c += 1
                    if c > 1 and p < 3:
                        print('CARRE')
                    if p > 1:
                        print("FULL")
            if b > 2:
                print("\033[33mFULL\033[0m")
                f += 1
            if f > 4:
                print('CARRE')

class TuchColor:

    def __init__(self, x, y, card3, card4, card5, card6, card7):
        self.x = x.get_Color()
        self.y = y.get_Color()
        self.card3 = carte3.get_Color()
        self.card4 = carte4.get_Color()
        self.card5 = carte5.get_Color()
        self.card6 = carte6.get_Color()
        self.card7 = carte7.get_Color()

    def get_Color_Hand(self, list=['', '', '', '', '']):
        self.list = [self.card3, self.card4, self.card5, self.card6, self.card7]
        if self.x == self.y:
            print('\033[36mCOULEUR en main\033[0m')

    def get_Color(self, list=['', '', '', '', '', '', '']):
        self.list = [self.x, self.y, self.card3, self.card4, self.card5, self.card6, self.card7]
        c = False
        s = False
        p = False
        t = False
        for couleur in self.list:
            if couleur == '♥':
                c += 1
                if c > 4:
                    print("\033[31mCouleur ♥\033[0m")
            if couleur == '♦':
                s += 1
                if s > 4:
                    print("\033[31mCouleur ♦\033[0m")
            if couleur == '♠':
                p += 1
                if p > 4:
                    print("\033[37mCouleur ♠\033[0m")
            if couleur == '♣':
                t += 1
                if t > 4:
                    print("\033[37mCouleur ♣\033[0m")




print('--------Tuch Player 1----------')
pairHand = TuchFlop(carte1, carte2, '', '', '', '', '')
pair = TuchFlop(carte1, carte2, '', '', '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop(carte1, carte2, '', '', '', '', '')
couleur = TuchColor(carte1, carte2, '', '', '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor(carte1, carte2, '', '', '', '', '')
couleur5.get_Color()
suite = TuchFlop(carte1, carte2, '', '', '', '', '')


print('--------Tuch Player 2----------')
pairHand = TuchFlop(carte8, carte9, '', '', '', '', '')
pair = TuchFlop(carte8, carte9, '', '', '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop(carte8, carte9, '', '', '', '', '')
couleur = TuchColor(carte8, carte9, '', '', '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor(carte8, carte9, '', '', '', '', '')
couleur5.get_Color()
suite = TuchFlop(carte8, carte9, '', '', '', '', '')


print('--------Tuch Player 3----------')
pairHand = TuchFlop(carte10, carte11, '', '', '', '', '')
pair = TuchFlop(carte10, carte11, '', '', '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop(carte10, carte11, '', '', '', '', '')
couleur = TuchColor(carte10, carte11, '', '', '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor(carte10, carte11, '', '', '', '', '')
couleur5.get_Color()
suite = TuchFlop(carte10, carte11, '', '', '', '', '')


print('--------Tuch Player 4----------')
pairHand = TuchFlop(carte12, carte13, '', '', '', '', '')
pair = TuchFlop(carte12, carte13, '', '', '', '', '')
pairHand.get_Paire_en_main()
pair.get_Paire()
pairFLOP = TuchFlop(carte12, carte13, '', '', '', '', '')
couleur = TuchColor(carte12, carte13, '', '', '', '', '')
couleur.get_Color_Hand()
couleur5 = TuchColor(carte12, carte13, '', '', '', '', '')
couleur5.get_Color()
suite = TuchFlop(carte12, carte13, '', '', '', '', '')
print('---------------------')
